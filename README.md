# My personal digital repository

List of software, apps, services, websites curated by me
Take into account this is still on progress and it will improve over time

## Contents
### - [Software](https://gitlab.com/masiv_/my-personal-digital-repository/-/blob/main/software.md)
### - [Websites](https://gitlab.com/masiv_/my-personal-digital-repository/-/blob/main/websites.md)
### - [Android](https://gitlab.com/masiv_/my-personal-digital-repository/-/blob/main/android.md)

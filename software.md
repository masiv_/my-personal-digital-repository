# Software
My own curated list of software

All software mentioned is Foss (Free and Open Software) unless specified **[x]**

#### Tools

- [CopyQ](https://github.com/hluk/CopyQ/releases) Clipboard manager
- [Obs](https://sourceforge.net/projects/obs-studio.mirror/files/latest/download) Recording and streaming software
- [Anydesk](https://anydesk.com/en) **[x]** Lightweight, easy remote control software
- [Handbrake](https://handbrake.fr/) Helpful program to transcode video on a lot of formats
- [Thunderbird](https://www.thunderbird.net/en-US/) E-mail organizer
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads) Virtual Machines
- [Youtube-dl-gui](https://github.com/MrS0m30n3/youtube-dl-gui/releases/tag/0.4) Youtube-dl (Download youtube videos, playlist,etc) GUI frontend
- [MusicBranz Picard](https://picard.musicbrainz.org/) Feature-rich mp3 tagger. Automatic titles, artist, album cover, date.
- [Cryptomator](https://cryptomator.org/downloads/) **[FREEMIUM]** Encrypt files file per file basis
- [Safing Portmaster](https://github.com/safing/portmaster)
- [Honeygain](https://www.honeygain.com/download/) **[x]** Pasive earnings using your connection
- [Duckdns](https://www.duckdns.org/) Dynamic DNS
- [Mymonero](https://github.com/mymonero/mymonero-app-js/releases) Monero offline wallet
- [Metasploit](https://www.metasploit.com/download) Feature-rich exploit tool

#### Discord stuff

- [Betterdiscord](https://betterdiscord.app/) Unofficial plugin and themer for Discord.
 - [Plugin Repo Downloader](https://betterdiscord.app/plugin/PluginRepo)
- [Fosscord]() No link yet, watch later
- [Premid](https://premid.app/downloads) **[x]** Connect your browser acticity with Discord Status

#### System information

- [Windirsat](https://www.fosshub.com/WinDirStat.html) Windows disk usage statistics viewer
- [Hwmonitor](https://www.cpuid.com/softwares/hwmonitor.html) **[x]** Hardware monitoring (voltage, temperatures, etc)
- [CpuZ](https://www.cpuid.com/softwares/cpu-z.html) **[x]** Windows system profiler 
- [GpuZ](https://www.techpowerup.com/download/techpowerup-gpu-z/) **[x]** Windows Gpu system profiler 
- [CristalDiskMark/CristalDiskInfo](https://crystalmark.info/en/download) **[x]** Disk speed and status

#### Geographic (Maps, GIS)

- [QGIS](https://www.qgis.org/en/site/forusers/download.html) GIS (Geographic information system) 
- [JOSM](https://josm.openstreetmap.de/) Feature-rich java OpenSreetMap editor

#### Design

- [Inkscape](https://inkscape.org/) Vectorial graphics editor
- [Kdenlive](https://kdenlive.org/en/download) Video editor
- [Krita](https://krita.org/en/download/krita-desktop/) Drawing software
- [Blender](https://www.blender.org/download/) 3d modeling software
- [FreeCad](https://www.freecadweb.org/downloads.php) CAD software
- [LibreCad](https://sourceforge.net/projects/librecad/) CAD Software
- [SweetHome3D](https://sourceforge.net/projects/sweethome3d/) CAD Software

#### Dev tools

- [Brackets](https://brackets.io/) Lightweight git editor
- [Git](https://gitforwindows.org/) 
- [Adb 15sec + scrcpy](https://forum.xda-developers.com/t/official-tool-windows-adb-fastboot-and-drivers-15-seconds-adb-installer-v1-4-3.2588979/) 


#### Android + PC integration

- [Superdisplay](https://superdisplay.app/) **[x]** Convert android device into external monitor via usb/wifi
- [Soundwire](https://georgielabs.net/) **[x]** Mirror sound to Android
- [Kde Connect]()

#### Games

- [EpicGames](https://www.epicgames.com/store/en-US/download) **[x]** Epic Games Launcher
- [Batlle.net](https://www.blizzard.com/en-us/download/) **[x]** Battle.net (Blizzard Games) launcher
- [RiotGames](https://signup.las.leagueoflegends.com/es/signup/redownload?page_referrer=index) **[x]** Riot Games Launcher
- [Genshin](https://genshin.mihoyo.com/en/download) **[x]** Genshin impact.
- [Beatconnect client](https://beatconnect.io/client) **[x]** Unofficial osu! beatmap downloader client
- [Tlauncher](https://tlauncher.org/en/) **[x]** Unoficcial minecraft cracked client
- [Hamachi](https://vpn.net/) **[x]** Connect on one network, useful for playing multiplayer old games.

#### Downloads/Streaming

- [Kodi](https://kodi.tv/download) Multimedia center
- [Stremio](https://stremio.com/) **[x]** Multimedia center 
- [Transmission](https://transmissionbt.com/download/) Lightweight torrent client.
- [Jdownloader + theme]() Download manager
 - [Link to unofficial guide to debloat Jdownloader](https://rentry.org/jdownloader2)
 
#### Windows specific stuff
This doesn't mean the software is only available on Windows, rather it only makes sense on Windows

- [All vcrdist downloader](https://www.techpowerup.com/download/visual-c-redistributable-runtime-package-all-in-one/) **[x]**
- [Winaero universal watermark disabler](https://winaero.com/download-universal-watermark-disabler/) **[x]** Windows watermark remover
- [TaskbarX](https://github.com/ChrisAnd1998/TaskbarX) Windows taskbar enhancer, center, translucent, etc
- [UltraUXThemePatcher](http://www.syssel.net/hoefs/software_uxtheme.php?lang=en) **[x]** 3rd party Windows patcher

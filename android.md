# Android apps

## Stores
- Apkpure
- Aurora Store
- F-droid

## Password managers
- Bitwarden
- KeepassDX
 - Syncthing recomended
 
## 2FA

- Aegis
- Steam autenthicator

## Browsers

- Bromite
- Icereaven
 - Addons support
- Kiwi


## Social media & messaging

- Barista
- Instander
- Element
- GB Whatsapp
- Signal
- Silence
- Slide
- Tutanota

## Music

- Dsub
- Subtracks
- Spotify Premium apk
- Shazam

## Media streaming

- Youtube
 - Vanced Manager (YT Vanced)
 - Newpipe
- Stremio
- Legión Anime
- Aniyomi
- Twire

## Android - PC integrations

- Soundwire
- Superdisplay
- KdeConnect



- Aliucord
- Ampere
- Aves
- PX Camera
- Kinemaster Pro
- Es File Explorer
- Anysoftkeyboard
- FlorisBoard
- Honeygain
- Libretorrent
- Lightroom
- Limbo x86 PC emulator
- Muzei
- Niagara Launcher
- Pixeldroid
- Privatelock
- Stellarium
- Universal AC Controller
- WifiMap
- WTMP

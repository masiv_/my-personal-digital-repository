# Websites 
Note that this section has lots of self refferences in megathreads between themselves

# Contents

# FREEMEDIAHECKYEAH

- [r/FREEMEDIAHECKYEAH](https://www.reddit.com/r/FREEMEDIAHECKYEAH/wiki)

# Piracy

- [r/Piracy megathread](https://www.reddit.com/r/Piracy/wiki/megathread)
- [Github - "Awesome piracy"](https://github.com/Igglybuff/awesome-piracy#plex)
- [r/OpenDirectories "All resources I know related to Open Directories"](https://www.reddit.com/r/opendirectories/comments/933pzm/all_resources_i_know_related_to_open_directories/)

#### Sites for downloading

- Books
 - [z-library](https://b-ok.lat/)
- Games
 - [r/PiratedGames megathread](https://rentry.org/pgames-mega-thread)
 - [r/CrackWatch](https://www.reddit.com/r/CrackWatch/)
 - [ElAmigos](https://elamigos.site/)
 - [FitGirl Repacks](https://fitgirl-repacks.site/)
 - [Ova Games](https://www.ovagames.com/)
 - [GOG Games](https://gog-games.com/)
 - [STEAMUNLOCKED](https://steamunlocked.net/)
 - [BlizzBoyGames](https://blizzboygames.net) Reuploads, spanish
- Torrents
 - [1337x.to](https://1337x.to) 

#### Some guides
- [THE COMPLETE GUIDE TO INSTALLING OFFICE FOR FREE!](https://rentry.org/officeguidefornoobs)

- [Convert Windows 10 Enterprise LTSC Evaluation to Windows 10 Enterprise LTSC 2019 Full](https://kb.teevee.asia/microsoft-windows/windows10-lstc-eval-to-ltsc-full/)

- [Github - Windows activation scripts (MAS)](https://github.com/massgravel/Microsoft-Activation-Scripts/)
- [Adobe Acrobat PRO DC download](https://rentry.co/Acrobat-Pro)

# Privacy, security, anonymity & FOSS

- [PrivacyTools.io](https://www.privacytools.io/)
- [Github - "Cool FOSS Android Apps"](https://github.com/albertomosconi/foss-apps)
- [The Hitchhiker's Guide to Online Anonymity](https://anonymousplanet.org/guide-dark.pdf)
- [GitHub - "Awesome Privacy"](https://github.com/pluja/awesome-privacy)
- [PRISM BREAK](https://prism-break.org/en/)

# Data hoarder

- [r/DataHoarder megathread](https://www.reddit.com/r/DataHoarder/wiki/index)

